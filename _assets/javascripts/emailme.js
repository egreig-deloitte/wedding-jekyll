function toggle_emf_element(element_obj,visible){
    toggle_element_with_validation(element_obj,visible);
    }

function toggle_element_with_validation(element_obj, visible, activate_validation_for_hidden_elements) {
    if (typeof activate_validation_for_hidden_elements == 'undefined') {
        activate_validation_for_hidden_elements = false;
    }
    element_obj.toggle(visible);
    if (!visible) {
        $.merge(element_obj, $("[class*=validate]", element_obj)).each(function(index, element) {
            var validation_class = get_validation_class(element);
            $(element).attr('temp_validation_def', get_validation_class(element)).removeClass(validation_class);
        });
        $(":input:not([type=button])", element_obj).each(function(index, element) {
            if ($(element).attr('temp_disabled') == null) {
                $(element).attr('temp_disabled', $(element).attr('disabled') == 'disabled' ? "1" : "0");
            }
            $(element).attr('disabled', 'disabled');
        });
        close_validation_prompt();
    } else {
        var target_objs = $.merge(element_obj, $("[temp_validation_def]", element_obj));
        if (!activate_validation_for_hidden_elements) {
            target_objs = target_objs.filter(":visible");
        }
        target_objs.each(function(index, element) {
            var temp_validation_def = $(element).attr("temp_validation_def");
            if (temp_validation_def) {
                $(element).addClass(temp_validation_def);
            }
        });
        $(":input:not([type=button])", element_obj).each(function(index, element) {
            if ($(element).attr('temp_disabled') == "1") {
                $(element).attr('disabled', 'disabled');
            } else {
                $(element).removeAttr('disabled');
            }
        });
    }
}

function close_validation_prompt(animation){
    if(typeof animation=='undefined'){
        animation=true;
    }
    if(animation){
        $.validationEngine.closePrompt(".formError",animation);
    }
    else {
        $(".formError").remove();
    }
}

(function($) {
    $.fn.validationEngine = function(settings) {
        if ($.validationEngineLanguage) {
            allRules = $.validationEngineLanguage.allRules;
        } else {
            $.validationEngine.debug("Validation engine rules are not loaded check your external file");
        }
        settings = jQuery.extend({
            allrules: allRules,
            validationEventTriggers: "focusout",
            inlineValidation: true,
            returnIsValid: false,
            liveEvent: false,
            unbindEngine: true,
            ajaxSubmit: false,
            scroll: true,
            promptPosition: "topRight",
            success: false,
            beforeSuccess: function() {},
            failure: function() {}
        }, settings);
        $.validationEngine.settings = settings;
        if (typeof($.validationEngine.ajaxValidArray) == "undefined") $.validationEngine.ajaxValidArray = new Array();
        if (settings.inlineValidation == true) {
            if (!settings.returnIsValid) {
                allowReturnIsvalid = false;
                if (settings.liveEvent) {
                    $(this).find("[class*=validate][type!=checkbox]").live(settings.validationEventTriggers, function(caller) {
                        _inlinEvent(this);
                    })
                    $(this).find("[class*=validate][type=checkbox]").live("click", function(caller) {
                        _inlinEvent(this);
                    })
                } else {
                    $(this).find("[class*=validate]").not("[type=checkbox]").bind(settings.validationEventTriggers, function(caller) {
                        _inlinEvent(this);
                    })
                    $(this).find("[class*=validate][type=checkbox]").bind("click", function(caller) {
                        _inlinEvent(this);
                    })
                }
                firstvalid = false;
            }

            function _inlinEvent(caller) {
                $.validationEngine.settings = settings;
                if ($.validationEngine.intercept == false || !$.validationEngine.intercept) {
                    $.validationEngine.onSubmitValid = false;
                    $.validationEngine.loadValidation(caller);
                } else {
                    $.validationEngine.intercept = false;
                }
            }
        }
        if (settings.returnIsValid) {
            if ($.validationEngine.submitValidation(this, settings)) {
                return false;
            } else {
                return true;
            }
        }
        $(this).bind("submit", function(event) {
            $.validationEngine.onSubmitValid = true;
            $.validationEngine.settings = settings;
            if ($.validationEngine.submitValidation(this, settings) == false) {
                if ($.validationEngine.submitForm(this, settings) == true) {
                    event.stopImmediatePropagation();
                    return false;
                }
            } else {
                settings.failure && settings.failure();
                event.stopImmediatePropagation();
                return false;
            }
        })
    };
    var PROMPT_OPACITY = 0.87;
    $.validationEngine = {
        defaultSetting: function(caller) {
            if ($.validationEngineLanguage) {
                allRules = $.validationEngineLanguage.allRules;
            } else {
                $.validationEngine.debug("Validation engine rules are not loaded check your external file");
            }
            settings = {
                allrules: allRules,
                validationEventTriggers: "blur",
                inlineValidation: true,
                returnIsValid: false,
                scroll: true,
                unbindEngine: true,
                ajaxSubmit: false,
                promptPosition: "topRight",
                success: false,
                failure: function() {}
            }
            $.validationEngine.settings = settings;
        },
        loadValidation: function(caller) {
            if (!$.validationEngine.settings) {
                $.validationEngine.defaultSetting()
            }
            rulesParsing = $(caller).attr('class');
            rulesRegExp = /\[(.*)\]/;
            getRules = rulesRegExp.exec(rulesParsing);
            str = getRules[1];
            pattern = /\[|,|\]/;
            result = str.split(pattern);
            var validateCalll = $.validationEngine.validateCall(caller, result)
            return validateCalll;
        },
        validateCall: function(caller, rules) {
            var promptTextArr = [];
            caller = caller;
            ajaxValidate = false;
            var callerName = $(caller).attr("name");
            $.validationEngine.isError = false;
            $.validationEngine.showTriangle = true;
            callerType = get_jquery_property(caller, "type");
            for (i = 0; i < rules.length; i++) {
                switch (rules[i]) {
                case "optional":
                    if (!$(caller).val()) {
                        $.validationEngine.closePrompt(caller);
                        return $.validationEngine.isError;
                    }
                    break;
                case "required":
                    _required(caller, rules);
                    break;
                case "custom":
                    _customRegex(caller, rules, i);
                    break;
                case "exemptString":
                    _exemptString(caller, rules, i);
                    break;
                case "ajax":
                    if (!$.validationEngine.onSubmitValid) {
                        _ajax(caller, rules, i);
                    };
                    break;
                case "length":
                    _length(caller, rules, i);
                    break;
                case "lengthWord":
                    _lengthWord(caller, rules, i);
                    break;
                case "lengthValue":
                    _lengthValue(caller, rules, i);
                    break;
                case "integerAndDecimal":
                    _integerAndDecimal(caller, rules, i);
                    break;
                case "maxCheckbox":
                    _maxCheckbox(caller, rules, i);
                    groupname = $(caller).attr("name");
                    caller = $("input[name='" + groupname + "']");
                    break;
                case "minCheckbox":
                    _minCheckbox(caller, rules, i);
                    groupname = $(caller).attr("name");
                    caller = $("input[name='" + groupname + "']");
                    break;
                case "minSelect":
                    _minSelect(caller, rules, i);
                    break;
                case "confirm":
                    _confirm(caller, rules, i);
                    break;
                case "funcCall":
                    _funcCall(caller, rules, i);
                    break;
                default:
                    ;
                };
            };
            radioHack();
            var promptText = getPromptTextByArr(promptTextArr);
            if ($.validationEngine.isError == true) {
                linkTofield = $.validationEngine.linkTofield(caller);
                ($("div." + linkTofield).size() == 0) ? $.validationEngine.buildPrompt(caller, promptText, "error") : $.validationEngine.updatePromptText(caller, promptText);
                $.validationEngine.updatePromptText(caller, promptText);
            } else {
                $.validationEngine.closePrompt(caller);
            }

            function radioHack() {
                if ($("input[name='" + callerName + "']").size() > 1 && (callerType == "radio" || callerType == "checkbox")) {
                    caller = $("input[name='" + callerName + "'][type!=hidden]:first");
                    $.validationEngine.showTriangle = true;
                }
            }

            function getPromptTextByArr(promptTextArr) {
                var result = "";
                for (var i = 0; i < promptTextArr.length; i++) {
                    if (!is_empty_str(promptTextArr)) {
                        result += "* " + promptTextArr[i] + "<BR>";
                    }
                }
                return result;
            }

            function _required(caller, rules) {
                callerType = get_jquery_property(caller, "type");
                if (callerType == "text" || callerType == "password" || callerType == "textarea" || callerType == "file") {
                    if (!$(caller).val()) {
                        $.validationEngine.isError = true;
                        promptTextArr.push($.validationEngine.settings.allrules[rules[i]].alertText);
                    }
                }
                if (callerType == "radio" || callerType == "checkbox") {
                    callerName = $(caller).attr("name");
                    if ($("input[name='" + callerName + "']:checked").size() == 0) {
                        $.validationEngine.isError = true;
                        if ($("input[name='" + callerName + "']").size() == 1) {
                            promptTextArr.push($.validationEngine.settings.allrules[rules[i]].alertTextCheckbox);
                        } else {
                            promptTextArr.push($.validationEngine.settings.allrules[rules[i]].alertTextCheckboxMultiple);
                        }
                    }
                }
                if (callerType == "select-one") {
                    if (!$(caller).val()) {
                        $.validationEngine.isError = true;
                        promptTextArr.push($.validationEngine.settings.allrules[rules[i]].alertText);
                    }
                }
                if (callerType == "select-multiple") {
                    if (!$(caller).find("option:selected").val()) {
                        $.validationEngine.isError = true;
                        promptTextArr.push($.validationEngine.settings.allrules[rules[i]].alertText);
                    }
                }
            }

            function _customRegex(caller, rules, position) {
                customRule = rules[position + 1];
                if (customRule && $.validationEngine.settings.allrules[customRule]) {
                    pattern = eval($.validationEngine.settings.allrules[customRule].regex);
                    if (!pattern.test($(caller).attr('value'))) {
                        $.validationEngine.isError = true;
                        promptTextArr.push($.validationEngine.settings.allrules[customRule].alertText);
                    }
                }
            }

            function _exemptString(caller, rules, position) {
                customString = rules[position + 1];
                if (customString == $(caller).attr('value')) {
                    $.validationEngine.isError = true;
                    promptTextArr.push($.validationEngine.settings.allrules['required'].alertText);
                }
            }

            function _funcCall(caller, rules, position) {
                customRule = rules[position + 1];
                params = rules.slice(position + 2);
                funce = $.validationEngine.settings.allrules[customRule].fname;
                var fn = window[funce];
                if (typeof(fn) === 'function') {
                    var fn_result = fn(caller, params);
                    var is_error = false;
                    var alert_text = $.validationEngine.settings.allrules[customRule].alertText;;
                    if (typeof fn_result === 'object') {
                        is_error = fn_result['is_error'];
                        if (fn_result['message']) {
                            alert_text = fn_result['message'];
                        }
                    } else {
                        is_error = fn_result;
                    }
                    if (is_error) {
                        $.validationEngine.isError = true;
                        promptTextArr.push(alert_text);
                    }
                }
            }

            function _ajax(caller, rules, position) {
                customAjaxRule = rules[position + 1];
                postfile = $.validationEngine.settings.allrules[customAjaxRule].file;
                fieldValue = $(caller).val();
                ajaxCaller = caller;
                fieldId = $.validationEngine.getMandatoryId(caller);
                ajaxValidate = true;
                ajaxisError = $.validationEngine.isError;
                if ($.validationEngine.settings.allrules[customAjaxRule].extraData) {
                    var tempExtraData = $.validationEngine.settings.allrules[customAjaxRule].extraData;
                    if (tempExtraData instanceof Function) {
                        extraData = tempExtraData();
                    } else {
                        extraData = tempExtraData;
                    }
                } else {
                    extraData = "";
                }
                if (!ajaxisError) {
                    $.ajax({
                        type: "POST",
                        url: postfile,
                        async: true,
                        data: "validateValue=" + fieldValue + "&validateId=" + fieldId + "&validateError=" + customAjaxRule + "&" + extraData,
                        beforeSend: function() {
                            if ($.validationEngine.settings.allrules[customAjaxRule].alertTextLoad) {
                                if (!$("div." + fieldId + "formError")[0]) {
                                    return $.validationEngine.buildPrompt(ajaxCaller, $.validationEngine.settings.allrules[customAjaxRule].alertTextLoad, "load");
                                } else {
                                    $.validationEngine.updatePromptText(ajaxCaller, $.validationEngine.settings.allrules[customAjaxRule].alertTextLoad, "load");
                                }
                            }
                        },
                        error: function(data, transport) {
                            $.validationEngine.debug("error in the ajax (_ajax): " + data.status + " " + transport)
                        },
                        success: function(data) {
                            data = eval("(" + data + ")");
                            ajaxisError = data.jsonValidateReturn[2];
                            customAjaxRule = data.jsonValidateReturn[1];
                            ajaxCaller = $("#" + data.jsonValidateReturn[0])[0];
                            fieldId = ajaxCaller;
                            ajaxErrorLength = $.validationEngine.ajaxValidArray.length;
                            existInarray = false;
                            if (ajaxisError == "false") {
                                _checkInArray(false)
                                if (!existInarray) {
                                    $.validationEngine.ajaxValidArray[ajaxErrorLength] = new Array(2);
                                    $.validationEngine.ajaxValidArray[ajaxErrorLength][0] = fieldId;
                                    $.validationEngine.ajaxValidArray[ajaxErrorLength][1] = false;
                                    existInarray = false;
                                }
                                $.validationEngine.ajaxValid = false;
                                promptTextArr.push($.validationEngine.settings.allrules[customAjaxRule].alertText);
                                var promptText = getPromptTextByArr(promptTextArr);
                                $.validationEngine.updatePromptText(ajaxCaller, promptText, "", true);
                            } else {
                                _checkInArray(true);
                                $.validationEngine.ajaxValid = true;
                                if ($.validationEngine.settings.allrules[customAjaxRule].alertTextOk) {
                                    $.validationEngine.updatePromptText(ajaxCaller, $.validationEngine.settings.allrules[customAjaxRule].alertTextOk, "pass", true);
                                } else {
                                    ajaxValidate = false;
                                    $.validationEngine.closePrompt(ajaxCaller);
                                }
                            }

                            function _checkInArray(validate) {
                                for (x = 0; x < ajaxErrorLength; x++) {
                                    if ($.validationEngine.ajaxValidArray[x][0] == fieldId) {
                                        $.validationEngine.ajaxValidArray[x][1] = validate;
                                        existInarray = true;
                                    }
                                }
                            }
                        }
                    });
                }
            }

            function _confirm(caller, rules, position) {
                confirmField = rules[position + 1];
                msg_order = rules[position + 2];
                if ($(caller).attr('value') != $("#" + confirmField).attr('value')) {
                    $.validationEngine.isError = true;
                    switch (msg_order) {
                    case "2":
                        promptTextArr.push($.validationEngine.settings.allrules["confirm"].alertText2);
                        break;
                    case "3":
                        promptTextArr.push($.validationEngine.settings.allrules["confirm"].alertText3);
                        break;
                    default:
                        promptTextArr.push($.validationEngine.settings.allrules["confirm"].alertText);
                        break;
                    }
                }
            }

            function _length(caller, rules, position) {
                startLength = eval(rules[position + 1]);
                endLength = eval(rules[position + 2]);
                feildLength = $(caller).attr('value').length;
                if (feildLength < startLength || feildLength > endLength) {
                    $.validationEngine.isError = true;
                    promptTextArr.push(parse_resource_variables($.validationEngine.settings.allrules["length"].alertText, {
                        min: startLength,
                        max: endLength
                    }));
                }
            }

            function _lengthWord(caller, rules, position) {
                startLength = eval(rules[position + 1]);
                endLength = eval(rules[position + 2]);
                feildLength = $.trim($(caller).attr('value')).split(' ').length;
                if (feildLength < startLength || feildLength > endLength) {
                    $.validationEngine.isError = true;
                    promptTextArr.push(parse_resource_variables($.validationEngine.settings.allrules["lengthWord"].alertText, {
                        min: startLength,
                        max: endLength
                    }));
                }
            }

            function _lengthValue(caller, rules, position) {
                startValue = eval(rules[position + 1]);
                endValue = eval(rules[position + 2]);
                fieldValue = $.trim($(caller).attr('value'));
                if (fieldValue < startValue || fieldValue > endValue || isNaN(fieldValue)) {
                    $.validationEngine.isError = true;
                    promptTextArr.push(parse_resource_variables($.validationEngine.settings.allrules["lengthValue"].alertText, {
                        min: startValue,
                        max: endValue
                    }));
                }
            }

            function _integerAndDecimal(caller, rules, position) {
                startValue = eval(rules[position + 1]);
                endValue = eval(rules[position + 2]);
                inputs = $(':input[name=' + escape_jquery_selector($(caller).attr('name')) + ']');
                intValue = $.trim(inputs.eq(0).val());
                decimalValue = $.trim(inputs.eq(1).val());
                if (is_empty_str(intValue)) {
                    intValue = 0;
                }
                if (is_empty_str(decimalValue)) {
                    decimalValue = 0;
                }
                fieldValue = parseFloat(intValue + '.' + decimalValue);
                if (fieldValue < startValue || fieldValue > endValue || isNaN(fieldValue)) {
                    $.validationEngine.isError = true;
                    promptTextArr.push(parse_resource_variables($.validationEngine.settings.allrules["lengthValue"].alertText, {
                        min: startValue,
                        max: endValue
                    }));
                } else {
                    $.each(inputs, function() {
                        $.validationEngine.closePrompt(this);
                    });
                }
            }

            function _maxCheckbox(caller, rules, position) {
                nbCheck = eval(rules[position + 1]);
                groupname = $(caller).attr("name");
                groupSize = $("input[name='" + groupname + "']:checked").size();
                if (groupSize > nbCheck) {
                    $.validationEngine.showTriangle = false;
                    $.validationEngine.isError = true;
                    promptTextArr.push(parse_resource_variables($.validationEngine.settings.allrules["maxCheckbox"].alertText, {
                        max: nbCheck
                    }));
                }
            }

            function _minCheckbox(caller, rules, position) {
                nbCheck = eval(rules[position + 1]);
                groupname = $(caller).attr("name");
                groupSize = $("input[name='" + groupname + "']:checked").size();
                if (groupSize < nbCheck) {
                    $.validationEngine.isError = true;
                    $.validationEngine.showTriangle = false;
                    promptTextArr.push(parse_resource_variables($.validationEngine.settings.allrules["minCheckbox"].alertText, {
                        min: nbCheck
                    }));
                }
            }

            function _minSelect(caller, rules, position) {
                nbCheck = eval(rules[position + 1]);
                id_name = $.validationEngine.getMandatoryId(caller);
                groupSize = $("select[id='" + id_name + "'] option:selected").size();
                if (groupSize < nbCheck) {
                    $.validationEngine.isError = true;
                    $.validationEngine.showTriangle = false;
                    promptTextArr.push(parse_resource_variables($.validationEngine.settings.allrules["minCheckbox"].alertText, {
                        min: nbCheck
                    }));
                }
            }
            return ($.validationEngine.isError) ? $.validationEngine.isError : false;
        },
        submitForm: function(caller) {
            if ($.validationEngine.settings.ajaxSubmit) {
                if ($.validationEngine.settings.ajaxSubmitExtraData) {
                    extraData = $.validationEngine.settings.ajaxSubmitExtraData;
                } else {
                    extraData = "";
                }
                $.ajax({
                    type: "POST",
                    url: $.validationEngine.settings.ajaxSubmitFile,
                    async: true,
                    data: $(caller).serialize() + "&" + extraData,
                    error: function(data, transport) {
                        $.validationEngine.debug("error in the ajax (submitForm): " + data.status + " " + transport)
                    },
                    success: function(data) {
                        if (data == "true") {
                            $(caller).css("opacity", 1)
                            $(caller).animate({
                                opacity: 0,
                                height: 0
                            }, function() {
                                $(caller).css("display", "none");
                                $(caller).before("<div class='ajaxSubmit'>" + $.validationEngine.settings.ajaxSubmitMessage + "</div>");
                                $.validationEngine.closePrompt(".formError", true);
                                $(".ajaxSubmit").show("slow");
                                if ($.validationEngine.settings.success) {
                                    $.validationEngine.settings.success && $.validationEngine.settings.success();
                                    return false;
                                }
                            })
                        } else {
                            data = eval("(" + data + ")");
                            if (!data.jsonValidateReturn) {
                                $.validationEngine.debug("you are not going into the success fonction and jsonValidateReturn return nothing");
                            }
                            errorNumber = data.jsonValidateReturn.length
                            for (index = 0; index < errorNumber; index++) {
                                fieldId = data.jsonValidateReturn[index][0];
                                promptError = data.jsonValidateReturn[index][1];
                                type = data.jsonValidateReturn[index][2];
                                $.validationEngine.buildPrompt(fieldId, promptError, type);
                            }
                        }
                    }
                })
                return true;
            }
            if (!$.validationEngine.settings.beforeSuccess()) {
                if ($.validationEngine.settings.success) {
                    if ($.validationEngine.settings.unbindEngine) {
                        $(caller).unbind("submit")
                    }
                    $.validationEngine.settings.success && $.validationEngine.settings.success();
                    return true;
                }
            } else {
                return true;
            }
            return false;
        },
        buildPrompt: function(caller, promptText, type, ajaxed) {
            if (!$.validationEngine.settings) {
                $.validationEngine.defaultSetting()
            }
            deleteItself = "." + $.validationEngine.getMandatoryId(caller) + "formError"
            if ($(deleteItself)[0]) {
                $(deleteItself).stop();
                $(deleteItself).remove();
            }
            var divFormError = document.createElement('div');
            var formErrorContent = document.createElement('div');
            linkTofield = $.validationEngine.linkTofield(caller)
            $(divFormError).addClass("formError")
            if (type == "pass") {
                $(divFormError).addClass("greenPopup")
            }
            if (type == "load") {
                $(divFormError).addClass("blackPopup")
            }
            if (ajaxed) {
                $(divFormError).addClass("ajaxed")
            }
            $(divFormError).addClass(linkTofield);
            $(formErrorContent).addClass("formErrorContent");
            $("body").append(divFormError);
            $(divFormError).append(formErrorContent);
            if ($.validationEngine.showTriangle != false) {
                var arrow = document.createElement('div');
                $(arrow).addClass("formErrorArrow");
                $(divFormError).append(arrow);
                if ($.validationEngine.settings.promptPosition == "bottomLeft" || $.validationEngine.settings.promptPosition == "bottomRight") {
                    $(arrow).addClass("formErrorArrowBottom")
                    $(arrow).html('<div class="line1"><!-- --></div><div class="line2"><!-- --></div><div class="line3"><!-- --></div><div class="line4"><!-- --></div><div class="line5"><!-- --></div><div class="line6"><!-- --></div><div class="line7"><!-- --></div><div class="line8"><!-- --></div><div class="line9"><!-- --></div><div class="line10"><!-- --></div>');
                }
                if ($.validationEngine.settings.promptPosition == "topLeft" || $.validationEngine.settings.promptPosition == "topRight") {
                    $(divFormError).append(arrow);
                    $(arrow).html('<div class="line10"><!-- --></div><div class="line9"><!-- --></div><div class="line8"><!-- --></div><div class="line7"><!-- --></div><div class="line6"><!-- --></div><div class="line5"><!-- --></div><div class="line4"><!-- --></div><div class="line3"><!-- --></div><div class="line2"><!-- --></div><div class="line1"><!-- --></div>');
                }
            }
            $(formErrorContent).html(promptText)
            callerTopPosition = $(caller).offset().top;
            callerleftPosition = $(caller).offset().left;
            callerWidth = $(caller).width();
            inputHeight = $(divFormError).height();
            var offset = null;
            if ($.validationEngine.settings.promptPosition == "topRight") {
                var minWidthOfPrompt = 130;
                var maxLeft = $(document.body).width() - minWidthOfPrompt;
                var targetLeftPosition = callerleftPosition + callerWidth - 30;
                if (targetLeftPosition > maxLeft) {
                    callerleftPosition = maxLeft;
                    offset = targetLeftPosition - maxLeft;
                } else {
                    callerleftPosition += callerWidth - 30;
                }
                callerTopPosition += -inputHeight - 10;
            }
            if ($.validationEngine.settings.promptPosition == "topLeft") {
                callerTopPosition += -inputHeight - 10;
            }
            if ($.validationEngine.settings.promptPosition == "centerRight") {
                callerleftPosition += callerWidth + 13;
            }
            if ($.validationEngine.settings.promptPosition == "bottomLeft") {
                callerHeight = $(caller).height();
                callerleftPosition = callerleftPosition;
                callerTopPosition = callerTopPosition + callerHeight + 15;
            }
            if ($.validationEngine.settings.promptPosition == "bottomRight") {
                callerHeight = $(caller).height();
                callerleftPosition += callerWidth - 30;
                callerTopPosition += callerHeight + 15;
            }
            $(divFormError).css({
                top: callerTopPosition,
                left: callerleftPosition,
                opacity: 0
            });
            if (offset) {
                var maxArrowOffset = $(divFormError).width() - 45;
                var arrowObj = $('.formErrorArrow', $(divFormError));
                var arrowLeft = (arrowObj.position().left + Math.min(offset, maxArrowOffset)) + "px";
                arrowObj.css({
                    left: arrowLeft
                });
            }
            $(divFormError).click(function() {
                $(this).remove();
            });
            return $(divFormError).animate({
                "opacity": PROMPT_OPACITY
            }, function() {
                return true;
            });
        },
        updatePromptText: function(caller, promptText, type, ajaxed) {
            linkTofield = $.validationEngine.linkTofield(caller);
            var updateThisPrompt = "." + linkTofield;
            if (type == "pass") {
                $(updateThisPrompt).addClass("greenPopup")
            } else {
                $(updateThisPrompt).removeClass("greenPopup")
            };
            if (type == "load") {
                $(updateThisPrompt).addClass("blackPopup")
            } else {
                $(updateThisPrompt).removeClass("blackPopup")
            };
            if (ajaxed) {
                $(updateThisPrompt).addClass("ajaxed")
            } else {
                $(updateThisPrompt).removeClass("ajaxed")
            };
            $(updateThisPrompt).find(".formErrorContent").html(promptText);
            callerTopPosition = $(caller).offset().top;
            inputHeight = $(updateThisPrompt).height();
            if ($.validationEngine.settings.promptPosition == "bottomLeft" || $.validationEngine.settings.promptPosition == "bottomRight") {
                callerHeight = $(caller).height();
                callerTopPosition = callerTopPosition + callerHeight + 15;
            }
            if ($.validationEngine.settings.promptPosition == "centerRight") {
                callerleftPosition += callerWidth + 13;
            }
            if ($.validationEngine.settings.promptPosition == "topLeft" || $.validationEngine.settings.promptPosition == "topRight") {
                callerTopPosition = callerTopPosition - inputHeight - 10;
            }
            $(updateThisPrompt).animate({
                top: callerTopPosition,
                "opacity": PROMPT_OPACITY
            });
        },
        getMandatoryId: function(caller) {
            var id = $(caller).attr("id");
            if (is_empty_str(id)) {
                $(caller).attr("id", "ValidationEngineAutoId_" + get_next_unique_id("ValidationEngineAutoId"));
            }
            id = $(caller).attr("id");
            return id;
        },
        linkTofield: function(caller) {
            linkTofield = $.validationEngine.getMandatoryId(caller) + "formError";
            linkTofield = linkTofield.replace(/\[/g, "");
            linkTofield = linkTofield.replace(/\]/g, "");
            return linkTofield;
        },
        closePrompt: function(caller, outside) {
            if (!$.validationEngine.settings) {
                $.validationEngine.defaultSetting()
            }
            if (outside) {
                $(caller).fadeTo("fast", 0, function() {
                    $(caller).remove();
                });
                return false;
            }
            if (typeof(ajaxValidate) == 'undefined') {
                ajaxValidate = false
            }
            if (!ajaxValidate) {
                linkTofield = $.validationEngine.linkTofield(caller);
                closingPrompt = "." + linkTofield;
                $(closingPrompt).fadeTo("fast", 0, function() {
                    $(closingPrompt).remove();
                });
            }
        },
        debug: function(error) {},
        submitValidation: function(caller) {
            var stopForm = false;
            $.validationEngine.ajaxValid = true;
            $(caller).find(".formError").remove();
            var toValidateSize = $(caller).find("[class*=validate]").size();
            $(caller).find("[class*=validate]").each(function() {
                linkTofield = $.validationEngine.linkTofield(this);
                if (!$("." + linkTofield).hasClass("ajaxed")) {
                    var validationPass = $.validationEngine.loadValidation(this);
                    return (validationPass) ? stopForm = true : "";
                };
            });
            ajaxErrorLength = $.validationEngine.ajaxValidArray.length;
            for (x = 0; x < ajaxErrorLength; x++) {
                if ($.validationEngine.ajaxValidArray[x][1] == false) {
                    $.validationEngine.ajaxValid = false;
                }
            }
            if (stopForm || !$.validationEngine.ajaxValid) {
                if ($.validationEngine.settings.scroll) {
                    destination = $(".formError:not('.greenPopup'):first").offset().top;
                    $(".formError:not('.greenPopup')").each(function() {
                        testDestination = $(this).offset().top;
                        if (destination > testDestination) {
                            destination = $(this).offset().top;
                        }
                    })
                    $("html:not(:animated),body:not(:animated)").animate({
                        scrollTop: destination
                    }, 1100);
                    if (typeof post_message_for_scroll_to_error != 'undefined') {
                        if (typeof emf_form_visit_id != 'undefined') {
                            post_message_for_scroll_to_error(emf_form_visit_id);
                        } else {
                            post_message_for_scroll_to_error();
                        }
                    }
                }
                return true;
            } else {
                return false;
            }
        }
    }
})(jQuery);;

(function($) {
    $.fn.validationEngineLanguage = function() {};
    $.validationEngineLanguage = {
        newLang: function() {
            $.validationEngineLanguage.allRules = {
                "required": {
                    "regex": "none",
                    "alertText": g_emf_resources['js_validation_required'],
                    "alertTextCheckboxMultiple": g_emf_resources['js_validation_required_select_multiple'],
                    "alertTextCheckbox": g_emf_resources['js_validation_required_checkbox']
                },
                "length": {
                    "regex": "none",
                    "alertText": g_emf_resources['js_validation_length']
                },
                "lengthWord": {
                    "regex": "none",
                    "alertText": g_emf_resources['js_validation_length_word']
                },
                "lengthValue": {
                    "regex": "none",
                    "alertText": g_emf_resources['js_validation_length_value']
                },
                "maxCheckbox": {
                    "regex": "none",
                    "alertText": g_emf_resources['js_validation_max_checkbox']
                },
                "minCheckbox": {
                    "regex": "none",
                    "alertText": g_emf_resources['js_validation_min_checkbox']
                },
                "confirm": {
                    "regex": "none",
                    "alertText": g_emf_resources['js_validation_confirm'],
                    "alertText2": g_emf_resources['js_validation_confirm2'],
                    "alertText3": g_emf_resources['js_validation_confirm3']
                },
                "telephone": {
                    "regex": "/^[0-9\-\(\)\+\ ]+$/",
                    "alertText": g_emf_resources['js_validation_telephone']
                },
                "email": {
                    "regex": "/^[a-zA-Z0-9.!#$%&'*+-/=?\\^_`{|}~-]+\\@([a-zA-Z0-9\\-]+\\.)+[a-zA-Z0-9]{2,4}$/",
                    "alertText": g_emf_resources['js_validation_email']
                },
                "url": {
                    "regex": /^((https?|ftp):\/\/)?(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i,
                    "alertText": g_emf_resources['js_validation_url']
                },
                "date": {
                    "regex": "/^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/",
                    "alertText": g_emf_resources['js_validation_date']
                },
                "onlyNumber": {
                    "regex": "/^[0-9\ ]+$/",
                    "alertText": g_emf_resources['js_validation_number_only']
                },
                "numberWithSign": {
                    "regex": "/^[0-9\ \+\-]+$/",
                    "alertText": g_emf_resources['js_validation_number_with_sign']
                },
                "extNumber": {
                    "regex": "/^[0-9,\.\ ]+$/",
                    "alertText": g_emf_resources['js_validation_ext_number']
                },
                "numberPercentSign": {
                    "regex": "/^[0-9,\.%]+$/",
                    "alertText": g_emf_resources['js_validation_number_percent_sign']
                },
                "noSpecialCaracters": {
                    "regex": "/^[0-9a-zA-Z]+$/",
                    "alertText": g_emf_resources['js_validation_no_special_chars']
                },
                "noSpecialCaractersEx": {
                    "regex": /^[0-9a-zA-Z\ \-\&\']+$/,
                    "alertText": g_emf_resources['js_validation_no_special_chars_ex']
                },
                "ajaxPassword": {
                    "file": get_site_url('account/check_password'),
                    "extraData": "Password=" + $('#Password').val(),
                    "alertTextOk": "Old Password is correct",
                    "alertTextLoad": "Loading, please wait",
                    "alertText": "Old Password is wrong"
                },
                "onlyLetter": {
                    "regex": "/^[a-zA-Z\ \']+$/",
                    "alertText": g_emf_resources['js_validation_letter_only']
                },
                "ajaxUsername": {
                    "file": get_site_url('account/check_user'),
                    "extraData": "Username=" + $('#Username').val(),
                    "alertTextOk": "This user is available",
                    "alertTextLoad": "Loading, please wait",
                    "alertText": "This user is already taken"
                },
                "ajaxEmail": {
                    "file": get_site_url('account/check_email'),
                    "extraData": "ContactEmail=" + $('#ContactEmail').val(),
                    "alertTextOk": "This email is available",
                    "alertTextLoad": "Loading, please wait",
                    "alertText": "This email is already taken"
                },
                "NameEmail": {
                    "fname": "NameEmail",
                    "alertText": "Invalid email address"
                },
                "valid_captcha": {
                    "fname": "valid_captcha",
                    "alertText": g_emf_resources['js_validation_captcha']
                },
                "url_ex": {
                    "fname": "url_ex",
                    "alertText": g_emf_resources['js_validation_url_ex']
                },
                "check_file_error": {
                    "fname": "check_file_error",
                    "alertText": g_emf_resources['js_validation_check_file_error']
                },
                "min_max": {
                    "fname": "validate_min_max",
                    "alertText": ""
                },
                "validate_gateway": {
                    "fname": "validate_gateway",
                    "alertText": ""
                },
                "val_not_free_plan": {
                    "fname": "val_not_free_plan",
                    "alertText": ""
                },
                "required_buyer_if_paypal": {
                    "fname": "required_buyer_if_paypal",
                    "alertText": ""
                },
                "non_negative_integer": {
                    "regex": "/^(0|([1-9][0-9]*))$/",
                    "alertText": "Must be an integer number"
                },
                "unique_field_value": {
                    "fname": "unique_field_value",
                    "alertText": "This value already exists."
                },
                "check_sum": {
                    "fname": "check_sum",
                    "alertText": ""
                },
                "ajaxMailChimpAPIKey": {
                    "file": get_site_url('manager/validate_mail_chimp_api_key'),
                    "extraData": "Username=" + $('#Username').val(),
                    "alertTextOk": "This user is available",
                    "alertTextLoad": "Loading, please wait",
                    "alertText": "This user is already taken"
                },
                "required_by_attr_flag": {
                    "fname": "required_by_attr_flag",
                    "alertText": ""
                }
            }
        }
    }
})(jQuery);

function prevent_duplicate_submission(form_obj) {
    form_obj.submit(function() {
        var property_name = 'emf_submitting_time';
        var submitting_time = form_obj.attr(property_name);
        var INTERVAL = 10 * 1000;
        if (submitting_time != null) {
            var waited_time = new Date().getTime() - submitting_time;
            if (waited_time < INTERVAL) {
                var rest_time = Math.round((INTERVAL - waited_time) / 1000);
                alert("Form data is being submitted, please wait. If there is a problem with the submission, please try again in " + rest_time + " seconds.");
                return false;
            }
        }
        form_obj.attr(property_name, new Date().getTime());
        return true;
    });
}

function highlight_field(li_obj, not_focus) {
    var editable = $(li_obj).hasClass("data_container");
    if (editable) {
        var class_name = 'highlight-field';
        $(li_obj).parent('ul').find('li').removeClass(class_name);
        $(li_obj).addClass(class_name);
    }
}

function highlight_field_on_focus() {
    highlight_field($(this).parents('li')[0], true);
}

function highlight_field_on_mousedown(event) {
    var not_focus = false;
    if (event.target && event.target.tagName) {
        not_focus = $.inArray(event.target.tagName.toLowerCase(), ['input', 'textarea', 'select']) > -1;
    }
    highlight_field(this, not_focus);
}

function is_empty_str(str) {
    return str == null || str == '';
}

function get_jquery_property(element, name) {
    return $(element).prop ? $(element).prop(name) : $(element).attr(name);
}

$(function(){
    toggle_emf_element($('#emf-li-1 .emf-allow-other input'), false);
    $('#emf-li-1').find('input:checked, .emf-allow-other input').change();
    toggle_emf_element($('#emf-li-3 .emf-allow-other input'), false);
    $('#emf-li-3').find('input:checked, .emf-allow-other input').change();
;

    $("#emf-form").validationEngine({
        validationEventTriggers:"blur",
        scroll:true
    });
    prevent_duplicate_submission($("#emf-form"));

    if($('#captcha_image').length>0){
        on_captcha_image_load();
    }

    $('.emf-field-grid td').click(function(event){
                if(!event.target.tagName || event.target.tagName.toLowerCase()!='td') return;

        $(this).find('input[type=checkbox],input[type=radio]').click();
    });


    $("#emf-form ul li").mousedown(highlight_field_on_mousedown);
    $("#emf-form ul li input, #emf-form ul li textarea, #emf-form ul li select").focus(highlight_field_on_focus);

        var form_obj=$("#emf-container form");
    if(form_obj.length>0 && form_obj.attr('action').indexOf('#')==-1 && window.location.hash){
        form_obj.attr('action', form_obj.attr('action')+window.location.hash);
    }
    });

var emf_widgets={text : 
            function(index){
                return $("#element_"+index).val();
            }
        ,number : 
            function(index){
                return $("#element_"+index).val();
            }
        ,textarea : 
            function(index){
                return $("#element_"+index).val();
            }
        ,new_checkbox : 
            function(index){
                var arr=new Array();
                $("input[name='element_"+index+"[]']:checked").each(function(){
                    arr[arr.length]=this.value;
                });
                var result=arr.join(", ");
                return result;
            }
        ,radio : 
            function(index){
                var result="";
                $("input[name=element_"+index+"]:checked").each(function(){
                    result=this.value;
                });
                return result;
            }
        ,select : 
            function(index){
                return $("#element_"+index).val();
            }
        ,email : 
            function(index){
                return $("#element_"+index).val();
            }
        ,phone : 
            function(index){
                var arr=new Array();
                $("input[id^=element_"+index+"_]").each(function(){
                    arr[arr.length]=this.value;
                });

                var result="";
                if(arr.length>0){
                    result=arr.join("-");
                }else{
                    result=$("#element_"+index).val();
                }
                return result;
            }
        ,datetime : 
            function(index){
                var result="";

                var date_part="";
                if($("#element_"+index+"_year").length==1){
                    date_part=$("#element_"+index+"_year-mm").val()+"/"+$("#element_"+index+"_year-dd").val()+"/"+$("#element_"+index+"_year").val();
                }

                var time_part="";
                if($("#element_"+index+"_hour").length==1){
                    time_part=$("#element_"+index+"_hour").val()+":"+$("#element_"+index+"_minute").val()+" "+$("#element_"+index+"_ampm").val();
                }

                if(date_part && time_part){
                    result=date_part+" "+time_part;
                }else{
                    result=date_part ? date_part : time_part;
                }

                return result;
            }
        ,url : 
            function(index){
                return $("#element_"+index).val();
            }
        ,file : 
            function(index){
                return $("#element_"+index).val();
            }
        ,Image : 
            function(index){
                return $("#element_"+index).val();
            }
        ,new_select_multiple : 
            function(index){
                return $("#element_"+index).val();
            }
        ,price : 
            function(index){
                var result="";
                var arr=new Array();
                $("input[id^=element_"+index+"_]").each(function(){
                    arr[arr.length]=this.value;
                });
                result=arr.join(".");
                return result;
            }
        ,hidden : 
            function(index){
                return $("#element_"+index).val();
            }
        ,unique_id : 
            function(index){
                return $("#element_"+index).val();
            }
        ,section_break : 
            function(index){
                return "";
            }
        ,page_break : 
            function(index){
                return "";
            }
        ,signature : 
            function(index){
                return $("#element_"+index).val();
            }
        ,star_rating : 
            function(index){
                var result="";
                $("input[name=element_"+index+"]:checked").each(function(){
                    result=this.value;
                });
                return result;
            }
        ,scale_rating : 
            function(index){
                var result="";
                $("input[name=element_"+index+"]:checked").each(function(){
                    result=this.value;
                });
                return result;
            }
        ,deprecated : 
            function(index){
                return $("#element_"+index).val();
            }
        ,address : 
            function(index){
                var result="";
                var element_arr=$("input,select").filter("[name='element_"+index+"[]']").toArray();
                result=element_arr[0].value+" "+element_arr[1].value+"\n"
                    +element_arr[2].value+","+element_arr[3].value+" "+element_arr[4].value+"\n"
                    +element_arr[5].value;
                return result;
            }
        ,name : 
            function(index){
                var arr=new Array();
                $("input[id^=element_"+index+"_]").each(function(){
                    arr[arr.length]=this.value;
                });
                var result=arr.join(" ");
                return result;
            }
        ,checkbox : 
            function(index){
                var arr=new Array();
                $("input[name='element_"+index+"[]']:checked").each(function(){
                    arr[arr.length]=this.value;
                });
                var result=arr.join(", ");
                return result;
            }
        ,select_multiple : 
            function(index){
                return $("#element_"+index).val();
            }
        };

var emf_condition_id_to_js_map={5 : 
            function(field_value, value){
                return field_value==value;
            }
        ,6 : 
            function(field_value, value){
                return field_value!=value;
            }
        ,1 : 
            function(field_value, value){
                return field_value.indexOf(value)>-1;
            }
        ,2 : 
            function(field_value, value){
                return field_value.indexOf(value)==-1;
            }
        ,3 : 
            function(field_value, value){
                return field_value.indexOf(value)==0;
            }
            ,4 : 
            function(field_value, value){
                return field_value.indexOf(value)==field_value.length-value.length;
            }
        ,7 : 
        function(field_value, value){
        return parseFloat(field_value)==parseFloat(value);
    }
    ,8 : 
            function(field_value, value){
                return parseFloat(field_value)>parseFloat(value);
            }
        ,9 : 
            function(field_value, value){
                return parseFloat(field_value) < parseFloat(value);
            }
        ,10 : 
            function(field_value, value){
                var date_for_field_value=Date.parse(field_value);
                var date_for_value=Date.parse(value);
                if(date_for_field_value && date_for_value){
                    return date_for_field_value == date_for_value;
                }
                return false;
            }
        ,11 : 
            function(field_value, value){
                var date_for_field_value=Date.parse(field_value);
                var date_for_value=Date.parse(value);
                if(date_for_field_value && date_for_value){
                    return date_for_field_value < date_for_value;
                }
                return false;
            }
        ,12 : 
            function(field_value, value){
                var date_for_field_value=Date.parse(field_value);
                var date_for_value=Date.parse(value);
                if(date_for_field_value && date_for_value){
                    return date_for_field_value > date_for_value;
                }
                return false;
            }
        };
var emf_group_to_field_rules_map=[];
var emf_group_to_page_rules_for_confirmation_map=[];

var emf_cart=null;
var emf_page_info={current_page_index: 0, page_element_index_min: 0, page_element_index_max: 6};
var emf_index_to_value_map=null;
var emf_form_visit_id="Ti014571ekKzMsdj24c";

var emf_index_to_option_map=[];