# -*- encoding: utf-8 -*-
# stub: bump 0.5.0 ruby lib

Gem::Specification.new do |s|
  s.name = "bump"
  s.version = "0.5.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Gregory Marcilhacy"]
  s.date = "2013-09-30"
  s.email = "g.marcilhacy@gmail.com"
  s.executables = ["bump"]
  s.files = ["bin/bump"]
  s.homepage = "https://github.com/gregorym/bump"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.2.0"
  s.summary = "Bump your gem version file"

  s.installed_by_version = "2.2.0" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rake>, ["~> 10.0.0"])
      s.add_development_dependency(%q<rspec>, ["~> 2.0"])
    else
      s.add_dependency(%q<rake>, ["~> 10.0.0"])
      s.add_dependency(%q<rspec>, ["~> 2.0"])
    end
  else
    s.add_dependency(%q<rake>, ["~> 10.0.0"])
    s.add_dependency(%q<rspec>, ["~> 2.0"])
  end
end
